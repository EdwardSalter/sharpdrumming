﻿using System.Reactive.Contrib.Monitoring;
using System.Threading.Tasks;

namespace SharpDrumming
{
    public static class RxMonitoring
    {
        private static Task<VisualRxInitResult> _initTask;

        public static void Init()
        {
            _initTask = VisualRxSettings.Initialize(
                VisualRxWcfDiscoveryProxy.Create()
                );
        }

        public static void WaitForReady()
        {
            _initTask.Wait();
        }

        public static void Enable()
        {
            VisualRxSettings.Enable = true;
        }

        public static void Disable()
        {
            VisualRxSettings.Enable = false;
        }
    }
}
