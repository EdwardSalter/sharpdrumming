﻿using System.Collections.Generic;
using System.Linq;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    class ChordListenerFactory
    {
        private static readonly Dictionary<IChordListener, string> Waves = new Dictionary<IChordListener, string>();

        public static Dictionary<IChordListener, string> WaveDictionary
        {
            get { return Waves; }
        }

        public static IEnumerable<IChordListener> CreateChordListeners(IInputPoller poller, DrumSet drumSet)
        {
            var listeners = drumSet.Drums.Select(
                drum =>
                {
                    var chordListener = new InputChordListener(poller, drum.Input);
                    Waves.Add(chordListener, drum.ShortDescription);
                    return chordListener;
                }).ToList();

            var dict = new Dictionary<JoystickOffset, List<IChordListener>>();
            foreach (var listener in listeners)
            {
                var minButton = listener.ButtonsToListenFor.Min();
                if (!dict.ContainsKey(minButton))
                {
                    dict.Add(minButton, new List<IChordListener>());
                }
                dict[minButton].Add(listener);
            }

            return dict.Select(keyValuePair => CreateChordListener(keyValuePair.Value));
        }

        private static IChordListener CreateChordListener(ICollection<IChordListener> chords)
        {
            if (chords.Count > 1)
            {
                return new ChordBlocker(chords);
            }
            return chords.Count != 0 ? chords.First() : null;
        }
    }
}
