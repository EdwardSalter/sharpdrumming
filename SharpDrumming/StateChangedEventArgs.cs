﻿using System;
using System.Collections.Generic;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    public class StateChangedEventArgs : EventArgs
    {
        public readonly IEnumerable<JoystickUpdate> Data;

        public StateChangedEventArgs(IEnumerable<JoystickUpdate> data)
        {
            Data = data;
        }
    }
}