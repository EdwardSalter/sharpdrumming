﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using ConsoleMenu;
using SharpDrumming.Properties;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    class Program
    {
        private static readonly WaveManager WaveManager = new WaveManager(Settings.Default.Volume);
        private static Mapping _mapping;
        private static Dictionary<IChordListener, string> _waves = new Dictionary<IChordListener, string>();
        private static VolumeChanger _volumeListener;
        private static int _volumeTextPositionTop;
        private static IEnumerable<IChordListener> _chordListeners;

        static void Main()
        {
            Console.Title = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

#if DEBUG
            RxMonitoring.Init();
#endif

            Joystick joystick = GetInputDevice();
            if (joystick == null)
            {
                Console.WriteLine("No input devices found. Exiting.");
                return;
            }

            string filename = GetMappingFile();
            _mapping = LoadMapping(filename);

            DrumSet drumSet = LoadDrumSet(_mapping.DrumSetFile);
            LoadDefaultWaves(WaveManager, drumSet.Drums);

            using (var poller = new InputPoller(joystick))
            {
                _volumeListener = SetupVolumeListener(poller, _mapping);
                SetupChordListeners(poller, drumSet);

#if DEBUG
                RxMonitoring.WaitForReady();
                RxMonitoring.Enable();
#endif
                poller.Start();

                Console.WriteLine("Using controller: {0}", joystick.Information.InstanceName);
                Console.WriteLine("Using drum set: {0}", drumSet.Name);
                // ReSharper disable StringLiteralTypo
                Console.WriteLine("Press Esc to exit");
                // ReSharper restore StringLiteralTypo

                _volumeTextPositionTop = Console.CursorTop;
                WriteVolumeString();
                Console.SetCursorPosition(0, _volumeTextPositionTop + 1);

                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                { }

                RxMonitoring.Disable();
            }

            foreach (var listener in _chordListeners)
            {
                listener.Dispose();
            }
            joystick.Dispose();
        }

        private static void WriteVolumeString()
        {
            int cursorLeft = Console.CursorLeft;
            int cursorTop = Console.CursorTop;

            Console.SetCursorPosition(0, _volumeTextPositionTop);
            Console.WriteLine("Volume: {0:f2}", _volumeListener.CurrentVolume);

            Console.SetCursorPosition(cursorLeft, cursorTop);
        }

        private static VolumeChanger SetupVolumeListener(IInputPoller poller, Mapping mapping)
        {
            var volumeListener = new VolumeChanger(poller, WaveManager);
            volumeListener.VolumeChanged += VolumeListenerOnVolumeChanged;
            volumeListener.VolumeUpButtonOffsets = mapping.VolumeUpButtons;
            volumeListener.VolumeDownButtonOffsets = mapping.VolumeDownButtons;

            return volumeListener;
        }

        private static void VolumeListenerOnVolumeChanged(object sender, EventArgs eventArgs)
        {
            WriteVolumeString();
        }

        private static string GetMappingFile()
        {
            var files = Directory.EnumerateFiles("mappings", "*.xml").ToList();

            var mappingMenu = new TypedMenu<string>(
                files,
                "Choose a drum set file",
                Path.GetFileName,
                Settings.Default.LastUsedMapping);
            var chosenMapping = mappingMenu.Display();

            SaveLastUsedMapping(chosenMapping);
            return chosenMapping;
        }

        private static void SetupChordListeners(IInputPoller poller, DrumSet drumSet)
        {
            _chordListeners = ChordListenerFactory.CreateChordListeners(poller, drumSet);

            _waves = ChordListenerFactory.WaveDictionary;

            foreach (var chordListener in _chordListeners)
            {
                chordListener.ChordPressed += ChordListenerOnChordPressed;
            }
        }

        private static void ChordListenerOnChordPressed(object sender, EventArgs eventArgs)
        {
            string waveKey = _waves[(IChordListener)sender];
            WaveManager.PlayWave(waveKey);
        }

        private static Mapping LoadMapping(string filename)
        {
            var serializer = new XmlSerializer(typeof(Mapping));
            using (XmlReader reader = new XmlTextReader(filename))
            {
                return (Mapping)serializer.Deserialize(reader);
            }
        }

        private static DrumSet LoadDrumSet(string filename)
        {
            var serializer = new XmlSerializer(typeof(DrumSet));
            using (XmlReader reader = new XmlTextReader(filename))
            {
                return (DrumSet)serializer.Deserialize(reader);
            }
        }

        private static void LoadDefaultWaves(WaveManager waveManager, IEnumerable<Drum> drums)
        {
            foreach (Drum drum in drums)
            {
                waveManager.LoadWave(drum.SoundFile, drum.ShortDescription);
            }
        }

        private static Joystick GetInputDevice()
        {
            using (var directInput = new DirectInput())
            {
                var devices = directInput.GetDevices(DeviceClass.All, DeviceEnumerationFlags.AttachedOnly);

                var deviceMenu = new TypedMenu<DeviceInstance>(
                    devices,
                    "Choose a controller",
                    device => device.InstanceName,
                    devices.FirstOrDefault(d => d.InstanceGuid == Settings.Default.LastUsedControllerID));
                var chosenDevice = deviceMenu.Display();
                return CreateJoystick(directInput, chosenDevice);
            }
        }

        // TODO: NO JOYSTICKS THIS FAR UP
        private static Joystick CreateJoystick(DirectInput directInput, DeviceInstance device)
        {
            var joystick = new Joystick(directInput, device.InstanceGuid);

            SaveLastUsedController(joystick.Information.InstanceGuid);

            return joystick;
        }

        private static void SaveLastUsedMapping(string file)
        {
            Settings.Default.LastUsedMapping = file;
            Settings.Default.Save();
        }

        private static void SaveLastUsedController(Guid controllerID)
        {
            Settings.Default.LastUsedControllerID = controllerID;
            Settings.Default.Save();
        }
    }
}
