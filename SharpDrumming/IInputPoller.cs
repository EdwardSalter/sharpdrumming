﻿using System;

namespace SharpDrumming
{
    public interface IInputPoller : IDisposable
    {
        event EventHandler<StateChangedEventArgs> ButtonStateChanged;
        void Start();
    }
}