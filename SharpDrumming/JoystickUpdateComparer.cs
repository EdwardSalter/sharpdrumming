﻿using System.Collections.Generic;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    public class JoystickUpdateComparer : IEqualityComparer<JoystickUpdate>
    {
        public bool Equals(JoystickUpdate x, JoystickUpdate y)
        {
            return x.Offset == y.Offset && x.Value == y.Value;
        }

        public int GetHashCode(JoystickUpdate obj)
        {
            return obj.Offset.GetHashCode() + obj.Value.GetHashCode();
        }
    }
}