﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    public class InputChordListener : IChordListener
    {
        private int NumberOfButtons
        {
            get { return ButtonsToListenFor.Count(); }
        }

        public IEnumerable<JoystickOffset> ButtonsToListenFor { get; private set; }
        private readonly IInputPoller m_poller;
        private readonly JoystickUpdateComparer m_joystickUpdateComparer = new JoystickUpdateComparer();

        // TODO: CHANGE TO EXPOSE OBSERVABLES AND SUBSCRIBE LATER IN THE CALL CHAIN
        public event EventHandler ChordPressed;
        public event EventHandler ChordReleased;

        private IDisposable m_pressedSubscription;
        private IDisposable m_releasedSubscription;

        public InputChordListener(IInputPoller poller, IEnumerable<JoystickOffset> buttonsToListenFor)
        {
            m_poller = poller;
            ButtonsToListenFor = buttonsToListenFor;

            StartListening();
        }

        private void StartListening()
        {
            var latestEvents = Observable.FromEventPattern<StateChangedEventArgs>(
                h => m_poller.ButtonStateChanged += h,
                h => m_poller.ButtonStateChanged -= h)
                .SelectMany(e => e.EventArgs.Data)
                //.Monitor("All events", 1)
                .DistinctUntilChanged(m_joystickUpdateComparer)
                //.Monitor("Duplicates removed", 1.5)
                .Where(e => ButtonsToListenFor.Contains(e.Offset))
                //.Monitor("Only for me", 2)
                .GroupBy(e => e.Offset)
                .Select(g => g.Scan(0, (i, update) => i + ValueToInt(update.Value)))
                //.MonitorMany("Per Button", 2.5, new string[]{})
                .Merge()
                //.Monitor("Merged", 3)
                .Scan(0, (count, current) => count + ValueToInt(current))
                .Monitor("Total Count", 4);

            var pressed = latestEvents
                .Where(group => group >= NumberOfButtons)
                .Monitor("Chord Pressed", 5);

            var released = latestEvents
                .SkipWhile(x => x < NumberOfButtons)
                .DistinctUntilChanged(x => x >= NumberOfButtons)
                .Where(group => group < NumberOfButtons)
                .Monitor("Chord Released", 6);

            m_pressedSubscription = pressed.Subscribe(_ => FireChordPressedEvent());
            m_releasedSubscription = released.Subscribe(_ => FireChordReleasedEvent());
        }

        private static int ValueToInt(int value)
        {
            if (value == ButtonState.Unpressed)
            {
                return -1;
            }
            return 1;
        }

        private void FireChordPressedEvent()
        {
            if (ChordPressed != null)
            {
                ChordPressed.Invoke(this, EventArgs.Empty);
            }
        }

        private void FireChordReleasedEvent()
        {
            if (ChordReleased != null)
            {
                ChordReleased.Invoke(this, EventArgs.Empty);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) return;

            m_pressedSubscription.Dispose();
            m_releasedSubscription.Dispose();
        }
    }
}
