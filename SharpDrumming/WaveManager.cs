﻿using System.Collections.Generic;
using System.Linq;
using SharpDX.IO;
using SharpDX.Multimedia;
using SharpDX.XAudio2;

namespace SharpDrumming
{
    public class WaveManager
    {
        private readonly XAudio2 m_xAudio;
        private readonly List<Wave> m_waves = new List<Wave>();
        private readonly MasteringVoice m_masteringVoice;

        public WaveManager(float initialVolume)
        {
            m_xAudio = new XAudio2(XAudio2Version.Default);
            m_masteringVoice = new MasteringVoice(m_xAudio);
            SetVolume(initialVolume);
            m_xAudio.StartEngine();
        }

        public void SetVolume(float volume)
        {
            m_masteringVoice.SetVolume(volume);
        }

        public void LoadWave(string path, string key)
        {
            var buffer = GetBuffer(path);
            m_waves.Add(new Wave { Buffer = buffer, Key = key });
        }

        public void PlayWave(string key)
        {
            var wave = m_waves.FirstOrDefault(x => x.Key == key);
            if (wave != null)
            {
                var voice = new SourceVoice(m_xAudio, wave.Buffer.WaveFormat, true);
                voice.SubmitSourceBuffer(wave.Buffer, wave.Buffer.DecodedPacketsInfo);
                voice.Start();
            }
        }

        private static AudioBufferAndMetaData GetBuffer(string soundFile)
        {
            var nativeFileStream = new NativeFileStream(soundFile, NativeFileMode.Open, NativeFileAccess.Read);
            var soundStream = new SoundStream(nativeFileStream);
            var buffer = new AudioBufferAndMetaData
            {
                Stream = soundStream.ToDataStream(),
                AudioBytes = (int)soundStream.Length,
                Flags = BufferFlags.EndOfStream,
                WaveFormat = soundStream.Format,
                DecodedPacketsInfo = soundStream.DecodedPacketsInfo
            };
            return buffer;
        }

        private sealed class AudioBufferAndMetaData : AudioBuffer
        {
            public WaveFormat WaveFormat { get; set; }
            public uint[] DecodedPacketsInfo { get; set; }
        }

        private class Wave
        {
            public AudioBufferAndMetaData Buffer { get; set; }
            public string Key { get; set; }
        }
    }
}
