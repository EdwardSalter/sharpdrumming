﻿using System;
using System.Collections.Generic;
using System.Timers;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    class InputPoller : IInputPoller
    {
        public event EventHandler<StateChangedEventArgs> ButtonStateChanged; 

        private const double DefaultPollingInterval = 10;

        private readonly Joystick m_joystick;

        private readonly Timer m_pollTimer = new Timer(DefaultPollingInterval);

        public InputPoller(Joystick joystick)
        {
            m_joystick = joystick;

            m_pollTimer.AutoReset = true;
            m_pollTimer.Elapsed += PollTimerOnElapsed;
        }

        public void Start()
        {
            m_joystick.Properties.BufferSize = 128;
            m_joystick.Acquire();
            
            m_pollTimer.Start();
        }

        private void PollTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            m_joystick.Poll();
            var data = m_joystick.GetBufferedData();
            LookForUpdatedState(data);
        }

        private void LookForUpdatedState(IEnumerable<JoystickUpdate> data)
        {
            if (ButtonStateChanged != null)
            {
                ButtonStateChanged.Invoke(this, new StateChangedEventArgs(data));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_joystick.Unacquire();
                m_pollTimer.Elapsed -= PollTimerOnElapsed;
                m_pollTimer.Dispose();
            }
        }
    }
}
