﻿using System;
using System.Collections.Generic;
using SharpDrumming.Properties;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    internal class VolumeChanger
    {
        public event EventHandler<EventArgs> VolumeChanged;

        private readonly IInputPoller m_poller;
        private readonly WaveManager m_waveManager;
        private IChordListener m_volumeDownListener;
        private IChordListener m_volumeUpListener;
        public float CurrentVolume { get; private set; }
        private const float VolumeStep = 0.1f;

        public IEnumerable<JoystickOffset> VolumeDownButtonOffsets
        {
            set
            {
                m_volumeDownListener = CreateChordListener(value);
            }
        }
        public IEnumerable<JoystickOffset> VolumeUpButtonOffsets
        {
            set
            {
                m_volumeUpListener = CreateChordListener(value);
            }
        }


        public VolumeChanger(IInputPoller poller, WaveManager waveManager)
        {
            m_waveManager = waveManager;
            m_poller = poller;
            CurrentVolume = Settings.Default.Volume;
        }

        private IChordListener CreateChordListener(IEnumerable<JoystickOffset> buttons)
        {
            var listener = new InputChordListener(m_poller, buttons);
            listener.ChordPressed += ListenerOnChordPressed;
            return listener;
        }

        private void ListenerOnChordPressed(object sender, EventArgs eventArgs)
        {
            if (sender == m_volumeDownListener)
            {
                ChangeVolume(VolumeChangeDirection.Down);
            }
            else if (sender == m_volumeUpListener)
            {
                ChangeVolume(VolumeChangeDirection.Up);
            }
        }

        private void ChangeVolume(VolumeChangeDirection direction)
        {
            CurrentVolume += (int)direction * VolumeStep;
            if (CurrentVolume <= 0)
            {
                CurrentVolume += VolumeStep;
            }

            m_waveManager.SetVolume(CurrentVolume);

            if (VolumeChanged != null)
            {
                VolumeChanged.Invoke(this, EventArgs.Empty);
            }
        }

        private enum VolumeChangeDirection
        {
            Up = 1,
            Down = -1
        }
    }
}