﻿using System;
using System.Collections.Generic;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    [Serializable]
    public class Mapping
    {
        public string DrumSetFile { get; set; }
        public List<JoystickOffset> VolumeUpButtons { get; set; }
        public List<JoystickOffset> VolumeDownButtons { get; set; }
    }
}
