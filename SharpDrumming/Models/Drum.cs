﻿using System;
using System.Collections.Generic;
using SharpDX.DirectInput;

namespace SharpDrumming
{
    [Serializable]
    public class Drum
    {
        public string SoundFile { get; set; }

        public List<JoystickOffset> Input { get; set; }

        public string ShortDescription { get; set; }
    }
}