﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpDrumming
{
    public class ChordBlocker : IChordListener
    {
        private readonly IEnumerable<IChordListener> m_chords;

        private readonly Dictionary<IChordListener, bool> m_chordsPressed = new Dictionary<IChordListener, bool>();

        private readonly object m_lockObject = new object();

        public event EventHandler ChordPressed;
        public event EventHandler ChordReleased;

        public ChordBlocker(IEnumerable<IChordListener> chords)
        {
            m_chords = chords;
            foreach (var chordListener in m_chords)
            {
                m_chordsPressed[chordListener] = false;
            }
            AttachChordListeners();
        }

        private void AttachChordListeners()
        {
            foreach (var listener in m_chords)
            {
                listener.ChordPressed += ListenerOnChordPressed;
                listener.ChordReleased += ListenerOnChordReleased;
            }
        }

        private void ListenerOnChordReleased(object sender, EventArgs eventArgs)
        {
            var chord = (IChordListener)sender;

            lock (m_lockObject)
            {
                m_chordsPressed[chord] = false;

                // Only send event if all chords are false
                bool sendEvent = m_chordsPressed.Values.All(f => !f);

                if (sendEvent && ChordReleased != null)
                {
                    ChordReleased.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private void ListenerOnChordPressed(object sender, EventArgs eventArgs)
        {
            var chord = (IChordListener)sender;

            lock (m_lockObject)
            {
                // Only send event if all chords are false
                bool sendEvent = m_chordsPressed.Values.All(f => !f);
                m_chordsPressed[chord] = true;

                if (sendEvent && ChordPressed != null)
                {
                    ChordPressed.Invoke(chord, EventArgs.Empty);
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) return;

            foreach (var listener in m_chords)
            {
                listener.Dispose();
            }
        }
    }
}
